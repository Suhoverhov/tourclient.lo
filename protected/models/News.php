<?php

/**
 * This is the model class for table "news".
 *
 * The followings are the available columns in table 'news':
 * @property string $id
 * @property string $title
 * @property string $content
 * @property string $date_update
 * @property integer $trash
 */
class News extends CActiveRecord {
    public $countNews;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'news';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('countNews, title, content', 'required', 'message' => 'Введите значение для поля "{attribute}".'),
            array('countNews', 'numerical', 'integerOnly' => true, 'message' => 'Значение в поле "{attribute}" должно быть числом.'),
            array('countNews', 'numerical', 'min' => 1, 'tooSmall' => 'Значение в поле "{attribute}" должно быть положительным.'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'title' => 'Заголовок',
            'content' => 'Content',
            'date_update' => 'Date Update',
            'trash' => 'Trash',
            'countNews' => 'Количество новостей'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('content', $this->content, true);
        $criteria->compare('date_update', $this->date_update, true);
        $criteria->compare('trash', $this->trash);

        return new CActiveDataProvider($this, array('criteria' => $criteria, 'pagination' => array('pageSize' => 20,),));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return News the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Перед добавдением записи в бд, устанавливаем значения для полей date_update и trash
     * @return bool
     */
    protected function beforeSave() {
        if (parent::beforeSave()) {
            if ($this->isNewRecord) {
                $this->date_update = date('Y-m-d H:i:s');
                $this->trash = 0;
            }
            return true;
        }
        else
            return false;
    }

    /**
     * Получить контент страницы
     * @param $adr
     * @return bool|mixed
     */
    public static function curlStart($adr) {
        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, $adr);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36");
            curl_setopt($curl, CURLOPT_AUTOREFERER, true);
            $curlExec = curl_exec($curl);
            curl_close($curl);
            return $curlExec;
        }
    }

    /**
     * Возвращает объект phpQuery
     * @param $ch
     * @return phpQueryObject|QueryTemplatesParse|QueryTemplatesSource|QueryTemplatesSourceQuery
     */
    public static function getQueryObj($ch) {
        $document = phpQuery::newDocumentHTML($ch);
        return pq($document);
    }

    public function launchParser($countNews) {

        $this->countNews = $countNews;
        $depthArchive = 0; // глубина архива

        while($countNews > 0) {
            $parseUrl = Yii::app()->params['parseUrl'] . '/archive/' . date('Ymd',strtotime('-' . $depthArchive . ' day'));
            $ch = News::curlStart($parseUrl);
            $pq = News::getQueryObj($ch);

            // получение и сохранение элементов каталога
            $newsList = $pq->find(".rubric_container .rubric_full .list div.list_item");

            foreach($newsList as $itemList) { // перебор страницы с новостями

                // при сохранении делаем вставку записи
                $this->setIsNewRecord(true);
                $this->id = null;

                $itemList = pq($itemList)->find(".list_item_content .list_item_text h3.list_item_title a")->attr('href');
                $newsUrl = Yii::app()->params['parseUrl'] . str_replace('//', '/', '/' . $itemList);

                $ch = News::curlStart($newsUrl);
                $pq = News::getQueryObj($ch);

                // получение и сохранение элементов каталога
                if (strpos($newsUrl, 'photolents')) {
                    $newsBody = $pq->find("#main .article_photo");
                } else {
                    $newsBody = $pq->find("#main .article");
                }

                $title =  pq($newsBody)->find(".article_header h1.article_header_title")->text();

                // проверяем, что спарсенная новость - уникальная
                if($this->findAllByAttributes(array('title'=>$title))) {
                    continue;
                }

                if (strpos($newsUrl, 'photolents')) {
                    $articleText = pq($newsBody)->find(".article_photo_description");
                    $articleText->find('.article_photo_gallery_wrap')->remove();
                } else {
                    $articleText = pq($newsBody)->find(".article_full .article_full_content .article_full_text:first-child");
                }

                $articleText->find('.article_illustration')->remove();
                $articleText->find('.inject-wrapper')->remove();
                $articleText->find('.article_inject_article-article')->remove();
                $articleText = $articleText->html();

                $articleText = $this->filteringText($articleText);

                // сохранение новости в бд
                if (!empty($title)) {

                    $this->attributes = array('title' => $title, 'content' => $articleText);
                    if($this->save()) {
                        $countNews--;
                    }
                }

                // завершаем парсинг, если количество спарсенных новостей равно нулю
                if ($countNews <= 0) {
                    break;
                }
            }
            $depthArchive++; // предыдущий день
        }

        return true;
    }

    /**
     * Фильтрует все символы в тексте согласно паттернам
     * @param $articleText
     * @return mixed|string
     */
    private function filteringText($articleText) {
        $articleText = strip_tags($articleText);

        $patterns = array(
            "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/", // URL
            "/[^@\s]*@[^@\s]*\.[^@\s]*/", // email
            "/[^A-Za-z0-9А-Яа-я]/ui", // все символы кроме букв английского и русского алфавитов и цифр
            "/\s–\s/i", // тире
            '/\s+/', // лишние пробелы
            "/\s+(\d+\s)+/i" // цифры окруженные пробелами
        );

        foreach($patterns as $item) {
            $articleText = preg_replace($item, ' ', $articleText);
        }

        $articleText = mb_strtolower(trim($articleText), 'UTF-8');

        return $articleText;
    }
}
