<?php

/**
 * This is the model class for table "words".
 *
 * The followings are the available columns in table 'words':
 * @property string $id
 * @property string $word
 * @property integer $reportId
 * @property string $frequency
 */
class Words extends CActiveRecord {
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'words';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('word, reportId, frequency', 'required'),
            array('reportId', 'numerical', 'integerOnly' => true),
            array('word', 'length', 'max' => 100),
            array('frequency', 'length', 'max' => 10),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array('id' => 'ID', 'word' => 'Слово', 'reportId' => 'Отчет', 'frequency' => 'Частота вхождения',);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Words the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
}
