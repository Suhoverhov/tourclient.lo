<?php

/**
 * This is the model class for table "reports".
 *
 * The followings are the available columns in table 'reports':
 * @property string $id
 * @property string $name
 * @property string $listNews
 * @property string $type
 */
class Reports extends CActiveRecord {
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'reports';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(array('name, listNews, type', 'required', 'message' => 'Поле "{attribute}" не должно быть пустым.'),
            array('name', 'length', 'max' => 120, 'min' => 4, 'tooShort' => 'Название должно состоять минимум из {min} символов.'),
            array('name', 'unique', 'message' => 'Такой отчет уже существует.'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array('id' => 'ID', 'name' => 'Название', 'listNews' => 'Список новостей', 'type' => 'Тип отчета',);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Reports the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Создание отчета частот встречаемости букв в текстах
     * @param $listNews
     * @return bool
     */
    public function  createLetterSReport($listNews) {

        if (is_array($listNews)) {
            $rows = News::model()->findAllByAttributes(array('id' => $listNews));
        } else {
            $rows = News::model()->findAll(array('select' => 'content'));
        }

        $result = array(
            'a' => 0,'b' => 0,'c' => 0,'d' => 0,'e' => 0,'f' => 0,'g' => 0,'h' => 0,'i' => 0,'j' => 0,'k' => 0,
            'l' => 0,'m' => 0,'n' => 0,'o' => 0,'p' => 0,'q' => 0,'r' => 0,'s' => 0,'t' => 0,'u' => 0,'v' => 0,
            'w' => 0,'x' => 0,'y' => 0,'z' => 0,'а' => 0, 'б' => 0, 'в' => 0, 'г' => 0, 'д' => 0, 'е' => 0, 'ё' => 0,
            'ж' => 0, 'з' => 0, 'и' => 0, 'й' => 0, 'к' => 0, 'л' => 0, 'м' => 0, 'н' => 0, 'о' => 0, 'п' => 0,
            'р' => 0, 'с' => 0, 'т' => 0, 'у' => 0, 'ф' => 0, 'х' => 0, 'ц' => 0, 'ч' => 0, 'ш' => 0, 'щ' => 0,
            'ъ' => 0, 'ы' => 0, 'ь' => 0, 'э' => 0, 'ю' => 0, 'я' => 0
        );
        foreach ($rows as $item) {
            preg_match_all('/[а-яa-z]/u', $item->content, $matches);
            foreach ($matches[0] as $letter) {
                    ++$result[$letter];
            }
        }

        $this->type = 'letter';
        if ($this->save()) {
            $insertArray = array();
            foreach($result as $key=>$item) {
                if($item !== 0) {
                    $insertArray[] = array('letter' => $key, 'reportId' => $this->id, 'frequency' => $item);
                }
            }
            Yii::app()->db->createCommand()->insert(Letters::model()->tableName(), $insertArray);
            return true;
        }

        return false;
    }

    /**
     * Строит отчет по частот встречаемости слов в выбранных текстах
     * @param $listNews
     * @return bool|Reports
     */
    public function createWordsReport($listNews) {

        if (is_array($listNews)) {
            $rows = News::model()->findAllByAttributes(array('id' => $listNews));
        } else {
            $rows = News::model()->findAll(array('select' => 'content'));
        }

        $allText = '';
        foreach ($rows as $item) {
            $allText .= $item->content . ' ';
        }

        $matches = preg_split('/ /u', $allText, -1, PREG_SPLIT_NO_EMPTY);
        $result = array();
        foreach ($matches as $word) {
            if (isset($result[$word])) {
                $result[$word] += 1;
            } else {
                $result[$word] = 1;
            }
        }

        $this->type = 'word';
        if ($this->save()) {

            $insertArray = array();
            $ct = 0;
            $bulk = 600; // размер пакет инсертов

            foreach($result as $key=>$item) {
                $insertArray[] = array('word' => $key, 'reportId' => $this->id, 'frequency' => $item);
                ++$ct;
                if ($ct >= $bulk) {
                    Yii::app()->db->createCommand()->insert(Words::model()->tableName(), $insertArray);
                    $insertArray = array();
                    $ct = 0;
                }
            }

            if (!empty($insertArray)) {
                Yii::app()->db->createCommand()->insert(Words::model()->tableName(), $insertArray);
            }
            return true;
        }

        return false;
    }
}
