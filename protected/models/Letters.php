<?php

/**
 * This is the model class for table "letters".
 *
 * The followings are the available columns in table 'letters':
 * @property string $id
 * @property string $letter
 * @property string $reportId
 * @property string $frequency
 */
class Letters extends CActiveRecord {
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'letters';
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'letter' => 'Буква',
            'reportId' => 'Отчет',
            'frequency' => 'Частота вхождения',
		);
	}

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Letters the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
