<?php
$this->breadcrumbs=array(
    'Отчеты' => array('index'),
    $model->name,
);

$this->menu=array(
    array('label' => 'Список отчетов', 'url' => array('index')),
    array('label' => 'Создать отчет по частоте встречаемости слов', 'url' => array('words')),
    array('label' => 'Создать отчет по частоте встречаемости букв', 'url' => array('letters')),
);
?>

<div class="page-header">
    <h1>Отчет "<?php echo $model->name; ?>"</h1>
</div>

<!-- Для отчетов по частоте встречаемости слов-->
<?php
if ($attr == 'word') {
    $gridColumns = array(
        array('name' => 'id', 'header' => 'id', 'filter' => false, 'htmlOptions' => array('style' => 'width: 60px')),
        array('name' => $attr, 'header' => $attr == 'word' ? 'Слово' : 'Буква'),
        array('name' => 'frequency'),
    );

    $this->widget('bootstrap.widgets.TbGridView',array(
        'filter' => $wordsLetterModel,
        'dataProvider'=> $wordsProvider,
        'columns'=> $gridColumns
    ));
}
?>

<!-- Для отчетов по частоте встречаемости букв-->
<?php if($attr == 'letter'):?>
<div id="chart1" style="height:1000px;"></div>
<script>
    var tempArray = <?php echo json_encode($letters); ?>;
    $.jqplot.config.enablePlugins = true;
    var s1 = <?php echo json_encode($frequency); ?>;
    var ticks = <?php echo json_encode($letters); ?>;

    plot1 = $.jqplot('chart1', [s1], {
        animate: !$.jqplot.use_excanvas,
        seriesDefaults:{
            renderer:$.jqplot.BarRenderer,
            pointLabels: { show: true },
            rendererOptions: {
                barDirection: 'horizontal',
                highlightMouseDown: true
            }
        },
        title: "График частот встречаемости букв",
        axes: {
            xaxis: {
                min:0, max:<?php echo max($frequency)?>
            },
            yaxis: {
                renderer: $.jqplot.CategoryAxisRenderer,
                ticks: ticks
            }
    },
        highlighter: { show: false }
    });

    $('#chart1').bind('jqplotDataClick',
        function (ev, seriesIndex, pointIndex, data) {
            $('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
        }
    );
</script>
<?php endif;?>