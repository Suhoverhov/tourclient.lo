<?php
$this->breadcrumbs=array(
    'Отчеты' => array('index'),
    'Создать отчет по частоте встречаемости слов',
);
$this->menu=array(
    array('label' => 'Список отчетов','url' => array('index')),
    array('label' => 'Создать отчет по частоте встречаемости слов','url' => array('words')),
    array('label' => 'Создать отчет по частоте встречаемости букв','url' => array('letters')),
);
?>

<div class="page-header">
    <h1>Создание отчета <small>по частоте встречаемости слов</small></h1>
</div>

<?php if(Yii::app()->user->hasFlash('successReportCreated')):
    echo Yii::app()->user->getFlash('successReportCreated');
endif; ?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'id' => 'reports-form',
    'enableAjaxValidation' => false,
    'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
    )
)); ?>

<?php echo $form->textFieldRow($modelReports,'name',array('class' => 'span5','maxlength' => 120)); ?>

<div id="array-hidden-checkbox">

</div>

<div >
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => 'Создать отчет',
    )); ?>
</div>

<?php $this->endWidget(); ?>

<p class="alert alert-warning">Отметьте в таблице, ниже, те тексты, которые нужно включить в отчет. Если ни один текст не выбран, то отчет будет строиться по ввсем текстам.</p>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
    'id' => 'reports-grid',
    'template' => '{items}{pager}',
    'dataProvider' => $model->search(),
    'afterAjaxUpdate' => 'function(){
            checkBoxOperations();
    }',
'columns' => array(
    'check' => array(
        'header' => '<input id="checkBox-create-all" type="checkbox" value="all"> Выбрать все',
        'type' => 'raw',
        'value' => function($data) {
                return '<input class="checkBox-create" type="checkbox" value="' . $data->id . '">';
            }
    ),
    'title',
),
)); ?>


