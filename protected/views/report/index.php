<?php
$this->breadcrumbs=array(
    'Список отчетов',
);

$this->menu=array(
    array('label' => 'Список отчетов','url' => array('index')),
    array('label' => 'Создать отчет по частоте встречаемости слов','url' => array('words')),
    array('label' => 'Создать отчет по частоте встречаемости букв','url' => array('letters')),
);
?>

<div class="page-header">
    <h1>Все отчеты</h1>
</div>

<?php
$gridColumns = array(
    array('name'=>'id', 'header'=>'id', 'filter' => false, 'htmlOptions' => array('style' => 'width: 60px')),
    array( 'name' =>'name', 'type' => 'raw', 'value' => function($data) {
            return '<a href="/report/view/id/' . $data->id . '">' . $data->name . '</a>';
        },
    ),
    array('name' => 'type', 'value' => function($data) {
            return $data->type == 'word' ? "слова" : "буквы";
        },)
);

$this->widget('bootstrap.widgets.TbGridView',
    array(
        'dataProvider' => $dataProvider,
        'template' => "{items}{pager}",
        'columns' => $gridColumns,
    )
); ?>
