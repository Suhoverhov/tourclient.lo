<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="en"/>

    <!-- blueprint CSS framework -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css"
          media="screen, projection"/>

    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css"
          media="screen, projection"/>
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css"/>

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

    <!-- mainmenu -->
    <?php $this->widget(
        'bootstrap.widgets.TbNavbar',
        array(
            'brand' => CHtml::encode(Yii::app()->name),
            'fixed' => false,
            'items' => array(
                array(
                    'class' => 'bootstrap.widgets.TbMenu',
                    'items' => array(
                        array('label' => 'Парсер', 'url' => array('/parser/index')),
                        array('label' => 'Отчеты', 'url' => array('/report/index')),
                    )
                )
            )
        )
    ); ?>
    <!-- mainmenu -->

    <?php if (isset($this->breadcrumbs)): ?>
        <?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
            'links' => $this->breadcrumbs,
            'homeLink' => CHtml::link(CHtml::encode(Yii::app()->name), Yii::app()->homeUrl)
        )); ?><!-- breadcrumbs -->
    <?php endif ?>

    <?php echo $content; ?>

    <div class="clear"></div>

    <div class="well" id="footer">
        Copyright &copy; <?php echo date('Y'); ?> LightSoft.<br/>
        Все права защищены.<br/>
    </div>
    <!-- footer -->

</div>
<!-- page -->

</body>
</html>
