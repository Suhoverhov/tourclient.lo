<?php
$this->breadcrumbs=array(
	'Парсер',
);
?>

<h1>Главное меню парсера</h1>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'id'=>'begin-parse',
    'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validationUrl' => Yii::app()->createUrl("parser/index" ),
        'ajaxVar' => 'ajax',
        'afterValidate' => "js: function(form, data, hasError) {
            //if no error in validation, send form data with Ajax
            if (! hasError) {
                $('#button-begin-parse').attr('disabled','disabled');
                $('#parser-loader').show();
                $('#data').hide();
                $.ajax({
                    type: 'POST',
                    url: '/parser/beginAjax',
                    data: $('#begin-parse').serialize(),
                    success: function(response) {
                        $('#data').html(response);
                        $('#parser-loader').hide();
                        $('#data').show();
                        $('#button-begin-parse').removeAttr('disabled');
                    }
                });
            }
            return false;
        }"
    ),
)); ?>

<?php echo $form->textFieldRow($model,'countNews',array('class' => 'span5', 'value' => 1)); ?>
<div>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'id' => 'button-begin-parse',
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => 'Начать парсинг',
    )); ?>
</div>
<?php $this->endWidget(); ?>

<div id="parser-loader"><img src="/images/loader.gif"></div>
<div id="data" class="alert alert-success"></div>
