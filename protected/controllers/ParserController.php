<?php

Yii::setPathOfAlias('vendor', __DIR__ . '/../vendor');
require_once Yii::getPathOfAlias('vendor') . '/phpQuery/phpQuery.php';

class ParserController extends Controller {
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/column2';


/**
* Lists all models.
*/
public function actionIndex() {
    set_time_limit (0);
    $model=new News;
    $this->performAjaxValidation($model);

    $data["model"] = $model;
    $this->render('index', $data);
}

/**
 * Lists all models.
 */
public function actionBeginAjax() {
    set_time_limit (0);
    $model=new News;
    $countNews = isset($_POST['News']['countNews']) ? $_POST['News']['countNews'] : 0;

    $data = array();
    if ($model->launchParser($countNews)) {
        $data['message'] = "Парсинг успешно завершен";
    }

    $data['model'] = $model;
    $this->renderPartial('_ajaxContent', $data, false, true);
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id) {
    $model=News::model()->findByPk($id);
    if ($model===null) {
        throw new CHttpException(404,'The requested page does not exist.');
    }

    return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model) {
    if (isset($_POST['ajax']) && $_POST['ajax']==='begin-parse') {
        echo CActiveForm::validate($model);
        Yii::app()->end();
    }
}

/**
 * Обработка ошибок
 */
public function actionError()
{
    if ($error=Yii::app()->errorHandler->error)
    {
        if (Yii::app()->request->isAjaxRequest)
            echo $error['message'];
        else
            $this->render('error', $error);
    }
}

}
