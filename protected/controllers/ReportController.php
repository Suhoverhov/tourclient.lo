<?php

class ReportController extends Controller {
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/column2';

/**
 * Регистрируем js-скрипты
 * @param CAction $action
 * @return bool
 */
public function beforeAction($action) {
    if (parent::beforeAction($action)) {
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile('/js/script.js');
        return true;
    }
    return false;
}


/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id) {
    $reportModel = Reports::model()->findByPk($id);

    if ($reportModel->type == 'word') {
        $type = 'Words';
        $attr = 'word';
        $wordsLetterModel = new Words();
    } else {
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile('/js/jqplot/excanvas.min.js', CClientScript::POS_HEAD, array('media' => 'lt IE 9'));
        $cs->registerScriptFile('/js/jqplot/jquery.jqplot.min.js');
        $cs->registerCssFile('/js/jqplot/jquery.jqplot.min.css');
        $cs->registerScriptFile('/js/jqplot/plugins/jqplot.barRenderer.min.js');
        $cs->registerScriptFile('/js/jqplot/plugins/jqplot.pointLabels.min.js');
        $cs->registerScriptFile('/js/jqplot/plugins/jqplot.categoryAxisRenderer.min.js');

        $this->layout = "//layouts/column1";
        $wordsLetterModel = new Letters();
        $type = 'Letters';
        $attr = 'letter';
        $lettersArray = $wordsLetterModel->findAllByAttributes(array('reportId' => $id, ));

        $letters = array();
        $frequency = array();
        foreach($lettersArray as $letter) {
            $letters[] = $letter->letter;
            $frequency[] = $letter->frequency;
        }
    }

    $wordsLetterModel->unsetAttributes(); // clear any default values
    if (isset($_GET[$type])) {
        $wordsLetterModel->attributes = $_GET[$type];
    }

    $criteria=new CDbCriteria;
    $criteria->compare('reportId',$id);
    $criteria->compare($attr, $wordsLetterModel->$attr, true);
    $criteria->compare('frequency', $wordsLetterModel->frequency);

    $data = array(
        'wordsProvider' => new CActiveDataProvider($wordsLetterModel, array(
                'criteria'=>$criteria,
                'pagination' => array('pageSize' => 20,),
                'sort' => array(
                    'attributes'=>array($attr, 'frequency'),
                ),
            )
        ),
        'model' => $this->loadModel($id),
        'wordsLetterModel' => $wordsLetterModel,
        'attr' => $attr,
        'letters' => isset($letters) ? $letters : 0,
        'frequency' =>  isset($frequency) ? $frequency : 0,
    );

    $this->render('view',$data);
}

/**
* Lists all models.
*/
public function actionIndex() {
    $modelReports = new Reports();
    $dataProvider=new CActiveDataProvider($modelReports, array('pagination' => array('pageSize' => 20,),));
    $this->render('index',array(
            'dataProvider'=>$dataProvider,
        )
    );
}

/**
 * Создание отчета по частоте встречаемости букв
 */
public function actionLetters() {
    set_time_limit (0);
    $modelReports = new Reports();
    $this->performAjaxValidation($modelReports);

    if (isset($_POST['Reports'])) {
        $start = microtime(true);

        $listNews = isset($_POST['Reports']['listNews']) ? $_POST['Reports']['listNews'] : 0;

        $_POST['Reports']['listNews'] = isset($_POST['Reports']['listNews']) ? json_encode($_POST['Reports']['listNews']): 0;
        $modelReports->attributes=$_POST['Reports'];

        $result = $modelReports->createLetterSReport($listNews);
        $time = microtime(true) - $start;

        if( $result ) {
            Yii::app()->user->setFlash('successReportCreated','<div class="alert alert-success">Отчет <a href="/report/view/id/'
                . $modelReports->id . '" class="alert-link">'
                . $modelReports->name . '</a> успешно создан. Время генерации - '
                . number_format($time,5) . ' секунд!</div>');
        }
    }

    $data = array(
        'model' => new News('search'),
        'modelReports' => new Reports('search'),
    );
    $this->render('letters',$data);
}

/**
* Создание отчета по частоте встречаемости слов
*/
public function actionWords() {
    set_time_limit (0);
    $modelReports = new Reports();
    $this->performAjaxValidation($modelReports);

    if (isset($_POST['Reports'])) {
        $start = microtime(true); // время начала генерации отчета

        $listNews = isset($_POST['Reports']['listNews']) ? $_POST['Reports']['listNews'] : 0;
        $_POST['Reports']['listNews'] = isset($_POST['Reports']['listNews']) ? json_encode($_POST['Reports']['listNews']): 0;
        $modelReports->attributes=$_POST['Reports'];

        $result = $modelReports->createWordsReport($listNews);

        $time = microtime(true) - $start; // общее время генерации отчета

        if($result) {
            Yii::app()->user->setFlash('successReportCreated','<div class="alert alert-success">Отчет <a href="/report/view/id/'
                . $modelReports->id . '" class="alert-link">'
                . $modelReports->name . '</a> успешно создан. Время генерации - '
                . number_format($time,5) . ' секунд!</div>');
        }
    }

    $model=new News('search');
    $model->unsetAttributes();  // clear any default values
    if (isset($_GET['Reports'])) {
        $model->attributes=$_GET['Reports'];
    }

    $modelReports=new Reports('search');
    $this->render('words',array(
    'model'=>$model,
    'modelReports'=>$modelReports,
    ));
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id) {
    $model = Reports::model()->findByPk($id);
    if ($model===null) {
        throw new CHttpException(404,'The requested page does not exist.');
    }

    return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model) {
    if (isset($_POST['ajax']) && $_POST['ajax']==='reports-form') {
        echo CActiveForm::validate($model);
        Yii::app()->end();
    }
}

}
