$( document ).ready(function() {
    checkBoxOperations();
});

function checkBoxOperations(){
    $('.checkBox-create').change(function(){
        if($(this).attr("checked") == "checked") {
            $("#array-hidden-checkbox").append('<input type="hidden" name="Reports[listNews][]" value="' + $(this).val() + '">')
        } else {
            $("#array-hidden-checkbox input[value=\'" + $(this).val() + "\']").remove();
        }
    });

    $('#checkBox-create-all').change(function(){
        if($(this).attr("checked") == "checked") {
            $("#reports-grid tr td input.checkBox-create").each(function(){
                if($(this).attr('checked') !== "checked") {
                    $(this).attr('checked', 'checked').trigger('change');
                }
            });
        } else {
            $("#reports-grid tr td input.checkBox-create").each(function(){
                $(this).removeAttr('checked').trigger('change');
            });
        }
    });

    $("#array-hidden-checkbox input[type=\'hidden\']").each(function(){
        var temp = $(this).val();
        $(".checkBox-create[value=" + temp + "]").attr("checked","checked");
    });
}